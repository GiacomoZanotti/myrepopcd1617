package pcd.lab09;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.flowables.ConnectableFlowable;
import io.reactivex.schedulers.Schedulers;

public class TestHotFlowableCreation {

	public static void main(String[] args) throws Exception {

		Flowable<String> source = Flowable.create(emitter -> {
		     
			new Thread(() -> {
				int i = 0;
				while (true){
					try {
						emitter.onNext("-> "+i);
						Thread.sleep(100);
						i++;
					} catch (Exception ex){}
				}
			}).start();

		     //emitter.setCancellable(c::close);

		 }, BackpressureStrategy.BUFFER);

		ConnectableFlowable<String> hotObservable = source.publish();
		hotObservable.connect();
		
		
		Thread.sleep(1000);
		
		hotObservable.subscribe(System.out::print); 
	
		Thread.sleep(10000);
	}

}
