package pcd.lab09;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class MyTest {

	public static void main(String[] args) {
		List<Integer> listintegers = new ArrayList<>();
		// Observable<String> reader = Observable.create(emitter -> {
		//
		// Thread t = new Thread(() -> {
		// for (int i = 0; i < 1000; i++) {
		// listintegers.add(i);
		// emitter.onNext(Integer.toString(i));
		//
		// }
		// });
		// System.out.println("starting...");
		// t.start();
		// });
		Observable<Integer> reader = Observable.create(new ObservableOnSubscribe<Integer>() {

			@Override
			public void subscribe(ObservableEmitter<Integer> subscriber) throws Exception {
				// TODO Auto-generated method stub
				Thread t = new Thread(() -> {
					int i = 0;
					while (i <= 10) {
						i++;
						subscriber.onNext(i);
					}

				});
				System.out.println("starting...");
				t.start();
				
			}
		});

		reader.subscribe((i) -> System.out.println("i numeri stampati sono: " + i));
	}

}
