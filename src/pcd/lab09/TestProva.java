package pcd.lab09;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.omg.CORBA.Current;

import com.fasterxml.jackson.annotation.JsonFormat.Value;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.disposables.Disposable;
import io.reactivex.flowables.ConnectableFlowable;
import pcd.ass03.ObservableTemperatureSensor;
import pcd.ass03.TemperatureSensorA;
import pcd.ass03.acme.TemperatureSensorB1;

public class TestProva {

	public static void main(String[] args) throws InterruptedException {

		ObservableTemperatureSensor s = new TemperatureSensorA();
		ConnectableFlowable<Double> obs1 = s.createObservable().publish();

		ObservableTemperatureSensor s2 = new TemperatureSensorB1();
		ConnectableFlowable<Double> obs2 = s2.createObservable().filter(value->value>0&&value<5000).publish();
		obs1.connect();
		obs2.connect();
		List<ConnectableFlowable<Double>> sensors = new ArrayList<>();
		sensors.add(obs1);
		sensors.add(obs2);

		// Flowable<Boolean> supervisor =
		// ConnectableFlowable.combineLatest(obs1, obs2,
		// (value1, value2) -> value1 > 20 && value2 > 20);
		// supervisor.subscribe(System.out::println);
//		ConnectableFlowable.combineLatest(sensors, (value) -> value[0] + " " + value[1]).subscribe(System.out::println);
//		ConnectableFlowable.combineLatest(sensors, (value) -> (Double) value[0] > 20.0 && (Double) value[1] > 20.0)
//				.subscribe(System.out::println);
		
		

	}
}