package pcd.ass01.ex2;

import java.util.ArrayList;
import java.util.List;

public class Counter {

	private int counter;

	private List<CounterObserver> observers;

	public Counter() {

		this.counter = 0;
		observers = new ArrayList<>();
	}

	public void update() {

		counter++;
		updateObversers();
	}

	public void addObserver(CounterObserver observer) {
		observers.add(observer);
	}

	public int getCounterCurrentValue() {
		return counter;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Counter" + counter;
	}

	private void updateObversers() {
		for (CounterObserver observer : observers) {
			observer.counterUpdated(this);
		}

	}

}
