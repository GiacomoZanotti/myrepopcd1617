package pcd.ass01.ex2;


import java.util.concurrent.Semaphore;



public class MyController implements Controller {

	private PingerPonger pinger, ponger;
	private Semaphore semaphore1, semaphore2, semaphore3;
	private Counter counter;
	private Viewer viewer;

	public MyController(Counter counter) {
		this.counter=counter;
		this.semaphore1=new Semaphore(2);
		this.semaphore2=new Semaphore(0);
		this.semaphore3=new Semaphore(1);
		this.pinger=new Pinger(this.counter,semaphore1,semaphore2);
		this.ponger=new Ponger(this.counter,semaphore2,semaphore3);
		this.viewer=new Viewer(semaphore1,semaphore2,semaphore3);
		this.counter.addObserver(viewer);
		
		
	}

	@Override
	public void action(String event) {
		if (event.compareToIgnoreCase("Start") == 0) {
			// TODO Auto-generated method stub
			viewer.start();
			pinger.start();
			ponger.start();
		

		}
		if (event.compareToIgnoreCase("stop") == 0) {
			pinger.switchOff();
			ponger.switchOff();
			viewer.shutdown();
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.exit(-1);
			

		}
	}

	public void start() {
		this.viewer.start();
	}

	
}
