package pcd.ass01.ex2;



public class Main {
	public static void main(String[] args) throws Exception {
		Counter counter = new Counter();
		Controller controller=new MyController(counter);
	
		VerySimpleView view=new VerySimpleView(controller);
		view.setVisible(true);

	}

}
