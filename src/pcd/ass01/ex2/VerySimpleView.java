package pcd.ass01.ex2;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextField;

class VerySimpleView extends JFrame implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Controller controller;
	public VerySimpleView(Controller controller) {
		super("My Very Simple View");

		this.controller = controller;

		setSize(400, 60);
		setResizable(false);

		JButton button1 = new JButton("Start");
		button1.addActionListener(this);

		JButton button2 = new JButton("Stop");
		button2.addActionListener(this);

		new JTextField(10);

		JPanel panel = new JPanel();
		panel.add(button1);
		panel.add(button2);

		setLayout(new BorderLayout());
		add(panel, BorderLayout.NORTH);

		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent ev) {
				System.exit(-1);
			}
		});
	}

	public void actionPerformed(ActionEvent ev) {

		try {
		
				controller.action(ev.getActionCommand());
		
		} catch (Exception ex){
			ex.printStackTrace();
		}

	}



}
