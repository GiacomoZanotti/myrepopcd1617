package pcd.ass01.ex2;

import java.util.concurrent.Semaphore;

public class Viewer extends Thread implements CounterObserver {

	private boolean off;
	private int state;
	private Semaphore s1, s2, s3;

	public Viewer(Semaphore s1, Semaphore s2, Semaphore s3) {
		this.s1 = s1;
		this.s2 = s2;
		this.s3 = s3;
		this.off = false;
	}

	public void run() {

		while (true) {
			try {
				s2.acquire();
				if (!off) {
					System.out.println("the counter current state is: " + state);
					s1.release();
					s3.release();

				} 
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public void shutdown() {
		this.off = true;
		System.out.println("Viewer is saying Goodbye!: the counter final state is: "+state);

	}

	@Override
	public void counterUpdated(Counter counter) {
		state = counter.getCounterCurrentValue();

	}

}
