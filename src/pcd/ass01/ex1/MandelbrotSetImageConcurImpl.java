package pcd.ass01.ex1;

public class MandelbrotSetImageConcurImpl implements MandelbrotSetImage {
	private int width, height;
	private Complex c0;
	private int image[];
	private Double delta;

	public MandelbrotSetImageConcurImpl(int width, int height, Complex c0, double rad0) {
		// TODO Auto-generated constructor stub
		this.width = width;
		this.height = height;
		this.image = new int[width * height];
		this.c0 = c0;
		this.delta = rad0 / (width * 0.5);
	}

	@Override
	public void compute(int nIterMax) {
		// TODO Auto-generated method stub
		try {
			//
			int lenght=Runtime.getRuntime().availableProcessors()+1;
			Worker[] workers=new Worker[lenght];
			int rangeOfPixelsToCompute=this.getWidth()/lenght;
			int fromX=0;
			int toX=rangeOfPixelsToCompute;
			for(int i=0;i<lenght-1;i++){
				workers[i]=new Worker(fromX,toX,this,nIterMax);
				workers[i].start();
				fromX=toX;
				toX+=rangeOfPixelsToCompute;
				
			}
			
			workers[lenght-1]=new Worker(fromX,this.getWidth(),this,nIterMax);
			Worker lastWorker=workers[lenght-1];
			lastWorker.start();
			for(int i=0;i<lenght;i++){
				workers[i].join();
			}
			

		} catch (Exception ex) {
		}
	}



	@Override
	public Complex getPoint(int x, int y) {
		// TODO Auto-generated method stub
		return new Complex((x - width * 0.5) * delta + c0.re(), c0.im() - (y - height * 0.5) * delta);
	}

	@Override
	public int getHeight() {
		// TODO Auto-generated method stub
		return height;
	}

	@Override
	public int getWidth() {
		// TODO Auto-generated method stub
		return width;
	}

	@Override
	public int[] getImage() {
		// TODO Auto-generated method stub

		return image;
	}
	public void update(int pixelPosition, int value){
		image[pixelPosition]=value;
	}



}
