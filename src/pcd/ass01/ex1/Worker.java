package pcd.ass01.ex1;

public class Worker extends Thread {
	private int fromX, toX, nIterations;
	private MandelbrotSetImageConcurImpl mandelbrotSet;

	
	public Worker(int fromX, int toX, MandelbrotSetImageConcurImpl mandelbrotSet, int nIterations) {
		super();
		this.fromX = fromX;
		this.toX = toX;
		
		this.nIterations = nIterations;
		this.mandelbrotSet = mandelbrotSet;

	}

	public void run() {
	//	System.out.println(this.getName() + " has started to compute pixels from " + fromX + " to " + toX);
		compute(nIterations);
	//	System.out.println(this.getName() + " has done");

	}

	public void compute(int nIterations) {
		for (int x = fromX; x < toX; x++) {
			for (int y = 0; y < mandelbrotSet.getHeight(); y++) {
				Complex c = mandelbrotSet.getPoint(x, y);
				double level = computeColor(c, nIterations);
				int color = (int) (level * 255);
				int pixelPosition =  y * mandelbrotSet.getWidth() + x;
				int value = color + (color << 8) + (color << 16);
				mandelbrotSet.update(pixelPosition, value);

			}
		}

	}

	private double computeColor(Complex c, int maxIteration) {
		
		int iteration = 0;
		Complex z = new Complex(0, 0);

		/*
		 * Repeatedly compute z := z^2 + c until either the point is out of the
		 * 2-radius circle or the number of iteration achieved the max value
		 * 
		 */
		while (z.absFast() <= 2 && iteration < maxIteration) { 
			z = z.times(z).plus(c);
			iteration++;
		}
		if (iteration == maxIteration) {
			/* the point belongs to the set */
			return 0;	
		} else {
			/* the point does not belong to the set => distance */
			return 1.0 - ((double) iteration) / maxIteration;
		}
	}

	public int getFromX() {
		return fromX;
	}

	public int getToX() {
		return toX;
	}



}
