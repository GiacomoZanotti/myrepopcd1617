package pcd.ass01.ex1;

public class Main {
	public static void main(String[] args) throws Exception {

		/* size of the mandelbrot set in pixel */
		int width = 4000;
		int height = 4000;

		/* number of iteration */
		int nIter = 500;
		
		/* region to be represented: center and radius */

		Complex c4 = new Complex(0.254036,0.000409); double rad4 = 0.001;

		/*
		 * Complex c0 = new Complex(0, 0); double rad0 = 2; time=7775 ms;
		 * 
		 * Complex c2 = new Complex(0.7485,0.0505); double rad2 = 0.5; time=16666 ms;
		 * 
		 * Complex c1 = new Complex(-0.75,0.1); double rad1 = 0.02; time=22879 ms;
		 * 
		 * Complex c2 = new Complex(0.7485,0.0505); double rad2 = 0.000002; time=629 ms;
		 * 
		 * Complex c3 = new Complex(0.254,0); double rad3 = 0.001; time=21292 ms;
		 * 
		 * Complex c4 = new Complex(0.254036,0.000409); double rad4 = 0.001; time=21794 ms;
		 */

		/* creating the set */
		MandelbrotSetImage set = new MandelbrotSetImageConcurImpl(width, height, c4, rad4);

		System.out.println("Computing...");
		StopWatch cron = new StopWatch();
		cron.start();

		/* computing the image */
		set.compute(nIter);
		cron.stop();
		System.out.println("done - " + cron.getTime() + " ms");

		MandelbrotView view = new MandelbrotView(set, 1200, 600);
		view.setVisible(true);
	}
}
