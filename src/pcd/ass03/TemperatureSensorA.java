package pcd.ass03;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import pcd.ass03.acme.TemperatureSensorA1;

public class TemperatureSensorA extends TemperatureSensorA1 implements ObservableTemperatureSensor {

	@Override
	public Flowable<Double> createObservable() {
		// TODO Auto-generated method stub
		return Flowable.create(new FlowableOnSubscribe<Double>() {

			@Override
			public void subscribe(FlowableEmitter<Double> subscriber) throws Exception {
				// TODO Auto-generated method stub

				new Thread(() -> {
					while (true) {
						subscriber.onNext(getCurrentValue());
						try {
							Thread.sleep(250);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}).start();
			}
		}, BackpressureStrategy.BUFFER);
	}

}
