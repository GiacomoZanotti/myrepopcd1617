package pcd.ass03;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import pcd.ass03.acme.TemperatureSensorB1;
import pcd.ass03.acme.TemperatureSensorB2;

public class MonitorView extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Controller controller;
	private List<JTextField> fields;
	private List<JLabel> labels;
	private JButton start;
	private JLabel alarm;
	private static final int HGAP = 50;
	private static final int VGAP = 50;

	public MonitorView(Controller controller) {
		this.controller = controller;
		fields = new ArrayList<>();
		labels = new ArrayList<>();
		setTitle("Monitor");
		setSize(700, 200);
		initGUI();
		setDefaultCloseOperation(EXIT_ON_CLOSE);

	}

	

	private void initGUI() {
		setLayout(new BorderLayout(HGAP, VGAP));
		JPanel panel1 = new JPanel(new FlowLayout());
		JPanel panel2 = new JPanel(new FlowLayout());
		start = new JButton("start");
		start.addActionListener(l -> this.controller.start(l,alarm));
		alarm = new JLabel("              ");
		createElements();
		for (int i = 0; i < controller.getSensors().size(); i++) {
			panel1.add(labels.get(i));
			panel1.add(fields.get(i));
		}

		panel2.add(start);
		panel2.add(alarm);
		add(panel1, BorderLayout.NORTH);
		add(panel2, BorderLayout.SOUTH);
	

	}

	private void createElements() {
		for (ObservableTemperatureSensor sensor : controller.getSensors()) {
			if (sensor instanceof TemperatureSensorA) {
				JTextField fieldA1 = new JTextField("Sensor A1                ");
				controller.setText(Sensor.SENSOR_A1, fieldA1);
				fieldA1.setEditable(false);
				fields.add(fieldA1);
				JLabel sensorA1 = new JLabel("A1");
				labels.add(sensorA1);
			}
			if (sensor instanceof TemperatureSensorB1) {
				JTextField fieldB1 = new JTextField("Sensor B1                ");
				fieldB1.setEditable(false);
				controller.setText(Sensor.SENSOR_B1, fieldB1);
				fields.add(fieldB1);
				JLabel sensorB1 = new JLabel("B1");
				labels.add(sensorB1);

			}
			if (sensor instanceof TemperatureSensorB2) {
				JTextField fieldB2 = new JTextField("Sensor B2                ");
				fieldB2.setEditable(false);
				controller.setText(Sensor.SENSOR_B2, fieldB2);
				fields.add(fieldB2);
				JLabel sensorB2 = new JLabel("B2");
				labels.add(sensorB2);
			}
		}
	}

}
