package pcd.ass03.ex1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import akka.actor.*;

public class Oracle extends UntypedActor {
	private long secretnumber;
	private int numberOfPlayers;
	private List<Guess> guesses;

	public Oracle(int numberOfPlayers, long secretnumber) {
		this.numberOfPlayers = numberOfPlayers;
		guesses = new ArrayList<>();
		this.secretnumber = secretnumber;

	}

	public static Props create(int numberOfPlayers, long secretnumber) {
		return Props.create(Oracle.class, () -> new Oracle(numberOfPlayers, secretnumber));
	}

	@Override
	public void onReceive(Object message) throws Throwable {
		// TODO Auto-generated method stub

		if (message instanceof Guess) {
			Guess msg = (Guess) message;
			guesses.add(msg);

		}
		if (everyoneTried()) {
			TurnCounter.getInstance().increment();
			Collections.sort(guesses);

			if (checkIfFound()) {
				System.out.println("\n FINAL TURN\n");
				printGuesses();
				for (Guess guess : guesses) {
					Result res = new MyResult(secretnumber, guess.getGuess());
					guess.getPlayer().tell(new GameOver(res.found()), getSelf());
				}
				getContext().stop(getSelf());
			} else {
				System.out.println("\n NEW TURN\n");
				printGuesses();

				for (Guess guess : guesses) {
					Result res = new MyResult(secretnumber, guess.getGuess());
					Answer resultmsg = new Answer(false, false);

					if (res.isGreater()) {
						resultmsg.setGreater(true);

					}
					if (res.isLess()) {
						resultmsg.setLess(true);
					}

					guess.getPlayer().tell(resultmsg, getSelf());

				}
				guesses.clear();

			}
		}
	}

	private boolean checkIfFound() {
		for (Guess guess : guesses) {
			Result res = new MyResult(secretnumber, guess.getGuess());
			if (res.found()) {
				Winner.getInstance().setWinner(guess.getPlayer().path().toString());
				return true;
			}

		}
		return false;
	}

	private boolean everyoneTried() {
		return guesses.size() == numberOfPlayers;
	}

	private void printGuesses() {
		for (Guess guess : guesses) {
			System.out.println(guess.toString());
		}
	}

}
