package pcd.ass03.ex1;

import java.util.ArrayList;
import java.util.List;

public class Winner {

	private String winner;
	private List<Observer> observers;
	private static Winner instance;

	private Winner() {
		observers = new ArrayList<>();
	}

	public static Winner getInstance() {
		if (instance == null) {
			instance = new Winner();
		}
		return instance;
	}

	public void setWinner(String winner) {
		this.winner = winner;
		notitfyObservers();
	}

	public void addObserver(Observer obs) {
		this.observers.add(obs);
	}

	public String getWinner() {
		return winner;
	}

	public void resetWinner() {
		winner="No winner yet";

	}

	private void notitfyObservers() {
		for (Observer obs : observers) {
			obs.updateWinner();
		}
	}

}
