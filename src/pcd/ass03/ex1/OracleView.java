package pcd.ass03.ex1;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class OracleView extends JFrame implements Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton secretNumberGenerator, start, stop, reset;
	private JTextField textSecretNumber, textNumberOfPlayers, textTurn, textWinner;
	private Controller controller;
	private Dimension buttonsDimension = new Dimension(150, 27);
	private int currentTurn;
	private String winner;
	private JLabel currentTurnLabel, winnerLabel;

	public OracleView(Controller controller) {
		super("Guess The Magic Number");
		this.controller = controller;
		this.setLayout(new BorderLayout());
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(800, 150);
		TurnCounter.getInstance().addObserver(this);
		Winner.getInstance().addObserver(this);
		InitGui();

	}

	private void InitGui() {

		JPanel panel1 = new JPanel(new FlowLayout());
		JPanel panel2 = new JPanel(new FlowLayout());
		JPanel panel3 = new JPanel(new FlowLayout());

		secretNumberGenerator = new JButton("create the number");
		start = new JButton("start");
		stop = new JButton("stop");
		reset = new JButton("reset");
		start.setPreferredSize(buttonsDimension);
		stop.setPreferredSize(buttonsDimension);
		reset.setPreferredSize(buttonsDimension);
		secretNumberGenerator.setPreferredSize(buttonsDimension);
		currentTurnLabel = new JLabel("Turn: ");
		winnerLabel = new JLabel("Winner:");
		textTurn = new JTextField(Integer.toString(currentTurn));
		textWinner = new JTextField("WINNER");
		textTurn.setEditable(false);
		textWinner.setEditable(false);
		textWinner.setPreferredSize(new Dimension(200,20));
		textSecretNumber = new JTextField("use the button to create a random number or type one");
		textSecretNumber.setEditable(true);
		textNumberOfPlayers = new JTextField("10");
		textNumberOfPlayers.setEditable(true);
		secretNumberGenerator.addActionListener(l -> this.controller.createSecretNumber(textSecretNumber));
		start.addActionListener(l -> this.controller.start(l, textNumberOfPlayers, textSecretNumber));
		stop.addActionListener(l -> this.controller.stop());
		reset.addActionListener(l -> this.controller.reset(textNumberOfPlayers, textSecretNumber));
		panel1.add(secretNumberGenerator);
		panel1.add(textSecretNumber);
		panel1.add(textNumberOfPlayers);
		add(panel1, BorderLayout.NORTH);
		panel2.add(start);
		panel2.add(stop);
		panel2.add(reset);
		add(panel2, BorderLayout.SOUTH);
		panel3.add(currentTurnLabel);
		panel3.add(textTurn);
		panel3.add(winnerLabel);
		panel3.add(textWinner);
		add(panel3, BorderLayout.CENTER);

	}

	@Override
	public void updateTurn() {
		// TODO Auto-generated method stub
		currentTurn = TurnCounter.getInstance().getTurnNumber();
		textTurn.setText(Integer.toString(currentTurn));
	
	}

	@Override
	public void updateWinner() {
		// TODO Auto-generated method stub
		winner=Winner.getInstance().getWinner();
		System.out.println("winner is: "+winner);
		textWinner.setText(winner);
		
	}

}
