package pcd.ass03.ex1;

public interface Observer {
	
	public void updateTurn();
	
	public void updateWinner();

}
