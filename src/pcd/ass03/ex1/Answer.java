package pcd.ass03.ex1;

public class Answer {

	private boolean isGreater;
	private boolean isLess;

	public Answer(boolean isGreater, boolean isLess) {
		this.isGreater = isGreater;
		this.isLess = isLess;
	}

	

	public boolean isGreater() {
		return isGreater;
	}

	public void setGreater(boolean isGreater) {
		this.isGreater = isGreater;
	}

	public boolean isLess() {
		return isLess;
	}

	public void setLess(boolean isLess) {
		this.isLess = isLess;
	}
	
	

}
