package pcd.ass03.ex1;


import akka.actor.ActorRef;

public class AskTicket {

	private ActorRef oracle;

	public AskTicket(ActorRef oracle) {
		this.oracle = oracle;
	}

	public ActorRef getOracle() {
		return oracle;
	}



}
