package pcd.ass03.ex1;


import java.awt.event.ActionEvent;

import javax.swing.JTextField;

public interface Controller {

	void start(ActionEvent event,JTextField fieldOfPlayers,JTextField fieldOfSecretNumber);

	void stop();

	void reset(JTextField fieldOfPlayers,JTextField fieldOfSecretNumber);

	void createSecretNumber(JTextField field);
	
}
