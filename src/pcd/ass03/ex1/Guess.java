package pcd.ass03.ex1;

import akka.actor.ActorRef;

public class Guess implements Comparable<Guess>{

	private long guess;
	private Ticket ticket;
	private ActorRef player;

	public Guess(long guess, Ticket ticket, ActorRef player) {
		this.guess = guess;
		this.ticket = ticket;
		this.player=player;
		
	}
	

	public ActorRef getPlayer() {
		return player;
	}


	public long getGuess() {
		return guess;
	}

	public Ticket getTicket() {
		return ticket;
	}
	
	public int getTicketNumber(){
		return ticket.getTicketNumber();
	}

	public String toString(){
		return "I'm player "+this.player.path()+", with guess "+guess +", and"+ticket.toString();
				
	}


	@Override
	public int compareTo(Guess o) {
		// TODO Auto-generated method stub
		if(this.getTicketNumber()>o.getTicketNumber()){
			return 1;
		}
		if(this.getTicketNumber()<o.getTicketNumber()){
			return -1;
		}
		else{
			return 0;
		}
	}

}
