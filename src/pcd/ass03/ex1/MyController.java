package pcd.ass03.ex1;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JTextField;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;

import scala.util.Random;

public class MyController implements Controller {

	private long secretNumber;
	private Random randomGenerator;
	private int numberOfPlayers;
	private List<ActorRef> players;
	private ActorRef oracle, dispenser;

	public MyController() {
		randomGenerator = new Random();
		players = new ArrayList<>();
	}

	@Override
	public void start(ActionEvent event, JTextField fieldOfPlayers, JTextField fieldOfSecretNumber) {
		// TODO Auto-generated method stub
		instantiateActors(fieldOfPlayers, fieldOfSecretNumber);

		((JButton) event.getSource()).setEnabled(false);

	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		shutdown();
		System.exit(0);
	}

	@Override
	public void reset(JTextField fieldOfPlayers, JTextField fieldOfSecretNumber) {
		// TODO Auto-generated method stub
		TurnCounter.getInstance().resetCounter();
		Winner.getInstance().resetWinner();
		shutdown();
		instantiateActors(fieldOfPlayers, fieldOfSecretNumber);

	}

	@Override
	public void createSecretNumber(JTextField field) {
		// TODO Auto-generated method stub
		secretNumber = Math.abs(randomGenerator.nextLong());

		field.setText(Long.toString(secretNumber));
	}

	private void shutdown() {
		for (ActorRef player : players) {
			player.tell(akka.actor.PoisonPill.getInstance(), ActorRef.noSender());
		}

		oracle.tell(akka.actor.PoisonPill.getInstance(), ActorRef.noSender());
		dispenser.tell(akka.actor.PoisonPill.getInstance(), ActorRef.noSender());

	}

	private void instantiateActors(JTextField fieldOfPlayers, JTextField fieldOfSecretNumber) {
		this.numberOfPlayers = Integer.parseInt(fieldOfPlayers.getText());
		this.secretNumber = Long.parseLong(fieldOfSecretNumber.getText());
		ActorSystem system = ActorSystem.create();
		oracle = system.actorOf(Oracle.create(numberOfPlayers, secretNumber));
		dispenser = system.actorOf(TicketDispenser.create(numberOfPlayers));
		for (int i = 0; i < numberOfPlayers; i++) {
			ActorRef player = system.actorOf(Player.create(dispenser), "player-" + (i+1));
			players.add(player);
			dispenser.tell(new AskTicket(oracle), player);
		}
	}

}
