package pcd.ass03.ex1;

public class Ticket {

	private int ticketNumber;

	public Ticket(int ticketNumber) {
		super();
		this.ticketNumber = ticketNumber;
	}

	public int getTicketNumber() {
		return ticketNumber;
	};

	public String toString(){
		return " ticket of number "+ticketNumber;
				
	}
	
}
