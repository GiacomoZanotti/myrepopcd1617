package pcd.ass03;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.functions.Predicate;
import pcd.ass03.acme.TemperatureSensorB1;
import pcd.ass03.acme.TemperatureSensorB2;



public class DataCollector {

	private List<ObservableTemperatureSensor> sensors;

	public DataCollector() {
		sensors = new ArrayList<>();
	}

	public List<ObservableTemperatureSensor> getSensors() {
		return sensors;
	}

	public DataCollector addSensor(ObservableTemperatureSensor sensor) {
		sensors.add(sensor);
		return this;
	}



	public ObservableTemperatureSensor getSensor(Sensor sensorType) {

		return Flowable.fromIterable(sensors).filter(new Predicate<ObservableTemperatureSensor>() {

			@Override
			public boolean test(ObservableTemperatureSensor sensor) throws Exception {
				// TODO Auto-generated method stub
				switch (sensorType) {
				case SENSOR_A1:
					return sensor instanceof TemperatureSensorA ? true : false;
				case SENSOR_B1:
					return sensor instanceof TemperatureSensorB1 ? true : false;
				case SENSOR_B2:
					return sensor instanceof TemperatureSensorB2 ? true : false;
				}
				return false;
			}
		}).blockingFirst();

	}

	
	
	
	
	

}
