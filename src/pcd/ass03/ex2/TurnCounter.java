package pcd.ass03.ex2;

import java.util.ArrayList;
import java.util.List;

public class TurnCounter {

	private int turnNumber;
	private List<Observer> observers;
	private static TurnCounter instance;

	private TurnCounter(int turnNumber) {
		this.turnNumber = turnNumber;
		observers = new ArrayList<>();
	}

	public static TurnCounter getInstance() {
		if (instance == null) {
			instance= new TurnCounter(0);
		}
		return instance;
	}

	public void increment() {
		turnNumber++;
		notitfyObservers();
	}

	public void addObserver(Observer obs) {
		this.observers.add(obs);
	}

	public int getTurnNumber() {
		return turnNumber;
	}

	public void resetCounter(){
		turnNumber=0;
	}
	private void notitfyObservers() {
		for (Observer obs : observers) {
			obs.updateTurn();
		}
	}

}
