package pcd.ass03.ex2;

import java.util.Random;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.DeliveryOptions;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.Json;

public class PlayerVertx extends AbstractVerticle {

	private int id;
	private long guess, min, max;
	private Random random;
	private DeliveryOptions options;

	public PlayerVertx(int id) {
		this.id = id;
		this.guess = -1;
		min = 0;
		max = Long.MAX_VALUE;
		random = new Random();
		options = new DeliveryOptions();
			

	}

	@Override
	public void start() throws Exception {
		// TODO Auto-generated method stub
		vertx.eventBus().consumer(ConsumerAddress.PLAYER, message -> getTicket(message));
		vertx.eventBus().consumer(ConsumerAddress.GAME_OVER, message -> {
			
			int pidWinner = (Integer) message.body();
			System.out.println(this.id == pidWinner ? "player " + this.id + " WON!" + guess : "SOB");
		
		});
	}

	public int getID() {
		return id;
	}

	private void getTicket(Message<Object> message) {
		if ((Integer) message.body() != this.id) {
			return;
		}
		guess = min + (max - min > 0 ? Math.abs(random.nextLong()) % (max - min) : max);
		vertx.eventBus().send(ConsumerAddress.ORACLE, guess, options.addHeader("pid", Json.encode(this.id)), reply -> {
			if (reply.succeeded()) {
				AnswerResult result = Json.decodeValue(reply.result().body().toString(), AnswerResult.class);
				switch (result) {
				case GREATER:
					min = guess;
					break;
				case LESS:
					max = guess;
					break;
				default:
					break;
				}
				vertx.eventBus().send(ConsumerAddress.DISPENSER, this.id);

			}
		});

	}

}
