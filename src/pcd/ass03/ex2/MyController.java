package pcd.ass03.ex2;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JTextField;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Vertx;
import scala.util.Random;

public class MyController implements Controller {

	private long secretNumber;
	private Random randomGenerator;
	private int numberOfPlayers;
	private List<PlayerVertx> players;
	private AbstractVerticle oracle, dispenser;
	private Vertx vertx;

	public MyController(Vertx vertx) {
		randomGenerator = new Random();
		players = new ArrayList<>();
		this.vertx = vertx;
	}

	@Override
	public void start(ActionEvent event, JTextField fieldOfPlayers, JTextField fieldOfSecretNumber) {
		// TODO Auto-generated method stub
		instantiateVerticles(fieldOfPlayers, fieldOfSecretNumber);
		sendPlayersFirstTicket();
		((JButton) event.getSource()).setEnabled(false);

	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		System.exit(0);
	}


	@Override
	public void createSecretNumber(JTextField field) {
		// TODO Auto-generated method stub
		secretNumber = Math.abs(randomGenerator.nextLong());

		field.setText(Long.toString(secretNumber));
	}


	private void instantiateVerticles(JTextField fieldOfPlayers, JTextField fieldOfSecretNumber) {
		this.numberOfPlayers = Integer.parseInt(fieldOfPlayers.getText());
		this.secretNumber = Long.parseLong(fieldOfSecretNumber.getText());
		this.oracle = new OracleVertx(secretNumber, numberOfPlayers);
		this.dispenser = new Dispenser(numberOfPlayers);
		vertx.deployVerticle(oracle);
		vertx.deployVerticle(dispenser);
		for (int i = 0; i < numberOfPlayers; i++) {
			PlayerVertx player = new PlayerVertx(i);
			players.add(player);
			vertx.deployVerticle(player);
		}
	}
	
	private void sendPlayersFirstTicket(){
		for (PlayerVertx player : players) {
			vertx.eventBus().publish(ConsumerAddress.PLAYER, player.getID());
		}
	}
	
	
	

}
