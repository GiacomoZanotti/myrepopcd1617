package pcd.ass03.ex2;

import java.util.ArrayList;
import java.util.List;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;

public class Dispenser extends AbstractVerticle {

	private int numberofplayers;
	private List<Message<Object>> requests;

	public Dispenser(int numberofplayers) {
		this.numberofplayers = numberofplayers;
		this.requests = new ArrayList<>();
	}

	public void start() {
		// TODO Auto-generated method stub
		vertx.eventBus().consumer(ConsumerAddress.DISPENSER, message -> handleRequests(message));
	}

	private void handleRequests(Message<Object> message) {
		requests.add(message);
		if (requests.size() == numberofplayers) {
			for (Message<Object> request : requests) {
				vertx.eventBus().publish(ConsumerAddress.PLAYER, (Integer) request.body());
			}
			requests.clear();
		}
		

	}

}
