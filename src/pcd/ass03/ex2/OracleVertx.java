package pcd.ass03.ex2;

import java.util.HashMap;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.Json;

public class OracleVertx extends AbstractVerticle {

	private long secretnumber;
	private HashMap<Integer, Long> pid_guess_map;
	private int numberOfPlayers;

	public OracleVertx(long secretnumber, int numberOfPlayers) {
		this.secretnumber = secretnumber;
		this.pid_guess_map = new HashMap<>();
		this.numberOfPlayers = numberOfPlayers;
	}

	@Override
	public void start() throws Exception {
		// TODO Auto-generated method stub
		vertx.eventBus().consumer(ConsumerAddress.ORACLE, message -> handleTicket(message));

	}

	private void handleTicket(Message<Object> message) {
		long guess = Long.parseLong(message.body().toString());
		int pid=Integer.parseInt(message.headers().get("pid"));
		pid_guess_map.put(pid, guess);
		if (pid_guess_map.size() == numberOfPlayers) {
			TurnCounter.getInstance().increment();
			printGuess();
			pid_guess_map.clear();
		}
		
		Result temp = new MyResult(secretnumber, guess);
		if (temp.isGreater()) {
			message.reply(Json.encode(AnswerResult.GREATER));
		}
		if (temp.isLess()) {
			message.reply(Json.encode(AnswerResult.LESS));
		}
		if (temp.found()) {
			vertx.eventBus().publish(ConsumerAddress.GAME_OVER, pid);
			Winner.getInstance().setWinner("player "+pid);
			
		}
	}

	private void printGuess() {
		System.out.println("\n NEW TURN\n");
		for (Integer pid : pid_guess_map.keySet()) {
			System.out.println("player " + pid + " tried to guess with " + pid_guess_map.get(pid));
		}
	}

}
