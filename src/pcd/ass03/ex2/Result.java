package pcd.ass03.ex2;

public interface Result {
	
	boolean found();
	boolean isGreater();	
	boolean isLess();
	

}
