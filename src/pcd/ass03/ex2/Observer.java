package pcd.ass03.ex2;

public interface Observer {
	
	public void updateTurn();
	
	public void updateWinner();

}
