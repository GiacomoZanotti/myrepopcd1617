package pcd.ass03.ex2;

public class MyResult implements Result {


	private long value;
	
	public MyResult(long secretNumber, long guess){
		this.value = (secretNumber - guess);
	}
	
	@Override
	public boolean found() {
		return value == 0;
	}

	@Override
	public boolean isGreater() {
		return value > 0;
	}

	@Override
	public boolean isLess() {
		return value < 0;
	}
	
	
}
