package pcd.ass03.ex2;

public class ConsumerAddress {

	public static final String PLAYER = "player";
	public static final String DISPENSER = "dispenser";
	public static final String ORACLE = "oracle";
	public static final String GAME_OVER = "gameover";

	private ConsumerAddress() {

	}

}
