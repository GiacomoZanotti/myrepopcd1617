package pcd.ass03;

import pcd.ass03.acme.TemperatureSensorB1;
import pcd.ass03.acme.TemperatureSensorB2;

public class Monitor {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		DataCollector dc = new DataCollector().addSensor(new TemperatureSensorA()).addSensor(new TemperatureSensorB1())
				.addSensor(new TemperatureSensorB2());
		MonitorView monitorView=new MonitorView(new MyController(dc));
		monitorView.setVisible(true);

	}

}
