package pcd.ass03;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;

import io.reactivex.flowables.ConnectableFlowable;
import pcd.ass03.acme.TemperatureSensorB1;
import pcd.ass03.acme.TemperatureSensorB2;

public class MyController implements Controller {

	private DataCollector dataCollector;
	private List<ConnectableFlowable<Double>> sensors;
	private Supervisor supervisor;

	public MyController(DataCollector dataCollector) {
		this.dataCollector = dataCollector;
		sensors = new ArrayList<>();
		supervisor = new Supervisor();
	}

	@Override
	public void start(ActionEvent event, JLabel alarm) {
		// TODO Auto-generated method stub
		for (ConnectableFlowable<Double> hotSensor : sensors) {
			hotSensor.connect();
		}
		((JButton) event.getSource()).setEnabled(false);
		supervisor.getFlowableSupervisor().subscribe(value -> {
			
			alarm.setText(value == true ? " ALARM" : "NO WORRIES");
		});

	}



	@Override
	public List<ObservableTemperatureSensor> getSensors() {
		// TODO Auto-generated method stub
		return dataCollector.getSensors();
	}

	@Override
	public ObservableTemperatureSensor setText(Sensor sensorType, JTextField field) {
		// TODO Auto-generated method stub
		ObservableTemperatureSensor sensor = dataCollector.getSensor(sensorType);
		ConnectableFlowable<Double> hotflowsensor = sensor.createObservable().filter(value -> value < 25).publish();
		if (sensor instanceof TemperatureSensorB2 || sensor instanceof TemperatureSensorB1) {
			supervisor.add(hotflowsensor);
		}
		hotflowsensor.subscribe(value -> {
			field.setText(Double.toString(value));
		});

		sensors.add(hotflowsensor);

		return sensor;

	}



}
