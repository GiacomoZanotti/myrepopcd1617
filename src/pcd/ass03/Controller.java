package pcd.ass03;

import java.awt.event.ActionEvent;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTextField;

public interface Controller {

	void start(ActionEvent event, JLabel alarm);

	ObservableTemperatureSensor setText(Sensor sensorType, JTextField field);

	List<ObservableTemperatureSensor> getSensors();

}
