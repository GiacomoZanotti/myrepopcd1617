package pcd.ass03;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.flowables.ConnectableFlowable;

public class Supervisor {

	private List<ConnectableFlowable<Double>> supervisedSensors;
	private Flowable<Boolean> supervisor;
	private final static int TEMPERATURE_THRESHOLD=20;

	public Supervisor() {
		supervisedSensors = new ArrayList<>();
	}

	public Flowable<Boolean> getFlowableSupervisor() {
		supervisor = ConnectableFlowable.combineLatest(supervisedSensors, values -> checkGreater(values));//values � un array di object
		return supervisor;

	}

	public void add(ConnectableFlowable<Double> supervisedSensor) {
		supervisedSensors.add(supervisedSensor);

	}
	
	private boolean checkGreater(Object[] values) {
		for (int i = 0; i < values.length; i++) {
			if ((Double) values[i] <TEMPERATURE_THRESHOLD) {
				return false;
			}
		}
		return true;
	}

}
