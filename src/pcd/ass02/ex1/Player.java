package pcd.ass02.ex1;

import java.util.Random;

/**
 * 
 * 
 * 
 * Player extends Thread: it keeps trying to guess the secret number until the
 * game is over
 *
 *
 */
public class Player extends Thread {

	private Oracle oracle;
	private long currentguess;
	private static final int MAX_BOUND = 100000;
	private static final int MIN_BOUND = 0;

	/**
	 * It is a player trying to guess the secret number. Its first guess is
	 * generated randomly
	 * 
	 * @param oracle
	 */
	public Player(Oracle oracle) {
		this.oracle = oracle;
		Random random = new Random();

		this.currentguess = (long) random.nextInt(MAX_BOUND - MIN_BOUND) + MIN_BOUND;

	}

	public void run() {

		Result res = null;

		while (!this.oracle.isGameFinished()) {

			try {
				res = this.oracle.tryToGuess((int) this.getId(), this.currentguess);

				computeNextGuess(res);

			} catch (GameFinishedException e) {
				// TODO Auto-generated catch block
				System.out.println(e.getMessage());
				res=e.getLastResult();

			}

		}
		System.out.println(res.found() ? this.getId() + " WON!!! " + currentguess
				: this.getId() + " SOB!! my last value was: " + currentguess);
		
	}

	/**
	 * It computes next guess by incrementing or decrementing, if the player
	 * current guess is respectively less or greater then the secret number
	 * 
	 * @param result
	 *            - The object wrapping the current guess
	 */
	private void computeNextGuess(Result result) {

		if (!this.oracle.isGameFinished()) {
			if (result.isGreater()) {
				currentguess--;
				System.out.println("Oracle: 'Hey " + this.getId() + ", your guess was too big!'"
						+ " Player: 'Ok I'll try with this new guess: " + currentguess + "'");
			}

			if (result.isLess()) {
				currentguess++;
				System.out.println("Oracle: 'Hey " + this.getId() + ", your guess was too little!'"
						+ " Player: 'Ok I'll try with this new guess: " + currentguess + "'");

			}

		}

	}
}
