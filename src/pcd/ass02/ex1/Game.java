package pcd.ass02.ex1;


public class Game {
	
	public static void main(String[]args){
		int available_processors=Runtime.getRuntime().availableProcessors()+1;
		Oracle oracle=new Oracle(available_processors);
		Player[]players=new Player[available_processors];
		
		for(Player p:players){
			p=new Player(oracle);
			p.start();
		}
	
		
		

	}

}
