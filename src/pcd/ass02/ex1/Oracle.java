package pcd.ass02.ex1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.Random;

/**
 * @author OPeratore
 * 
 *         Monitor controller of the game: it synchronises a given number of
 *         player, such that they can only try to make a guess once each turn
 *
 */
public class Oracle implements OracleInterface {

	private long secretnumber;
	private boolean gameover, finalturn;
	private int number_of_players;
	private HashMap<Integer, Long> map;
	private static final int MAX_BOUND = 100000;
	private static final int MIN_BOUND = 0;

	/**
	 * Oracle monitor used to synchronize players such that they can only make
	 * one and only guess each turn
	 * 
	 * @param number_of_players
	 */
	public Oracle(int number_of_players) {
		Random random = new Random();
		this.secretnumber = (long) random.nextInt(MAX_BOUND - MIN_BOUND) + MIN_BOUND;
		File file = new File("output.txt");
		try {
			PrintStream printStream = new PrintStream(file);
			printStream.println(secretnumber);
			printStream.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		this.gameover = false;
		this.finalturn = false;
		this.number_of_players = number_of_players;
		this.map = new HashMap<>();
	}

	@Override
	public synchronized boolean isGameFinished() {
		// TODO Auto-generated method stub
		return gameover;
	}

	@Override
	public synchronized Result tryToGuess(int playerId, long value) throws GameFinishedException {

		Result res = new MyResult(value, secretnumber);
		if (!hasAlreadyTried(playerId)) {
			this.map.put(playerId, value);

		}

		if (res.found()) {
			this.finalturn = true;

		}

		this.turnHandler(res);

		while (hasAlreadyTried(playerId)) {
			try {
				wait();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return res;
	}

	/**
	 * @param thread_ID
	 * 
	 * @return true if the player has tried to guess in this turn, false
	 *         otherwise
	 */
	private boolean hasAlreadyTried(int thread_ID) {
		return this.map.containsKey(thread_ID);
	}

	/**
	 * @return true if all the players tried to guess in this turn,false
	 *         otherwise
	 */
	private boolean everyoneTried() {
		return this.map.size() == this.number_of_players;
	}

	/**
	 * It checks if the player making a guess is the last one of the turn: if it
	 * is, it clears the map, notify waiting threads and then a new turn starts
	 * 
	 * 
	 * @throws GameFinishedException
	 *             if it is the last turn and everyone tried to make a guess
	 * 
	 * 
	 */
	private void turnHandler(Result result) throws GameFinishedException {
		if (everyoneTried() && !finalturn) {
			System.out.println("\n NEW TURN! \n    ");
			this.map = new HashMap<>();
			notifyAll();
		} else if (everyoneTried() && finalturn) {
			this.gameover = true;
			this.map = new HashMap<>();
			notifyAll();
			throw new GameFinishedException("\nGAME OVER\n",result);
			
		}

	}

}
